msgid ""
msgstr ""
"Project-Id-Version: Google Doc Embedder\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-05-25 10:14-0600\n"
"PO-Revision-Date: 2012-05-25 10:14-0600\n"
"Last-Translator: Kevin Davis <kev@tnw.org>\n"
"Language-Team: Kevin Davis <kev@tnw.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: .\n"

#: gde-functions.php:32
msgid "Download"
msgstr ""

#: gde-functions.php:344
#: gviewer.php:292
msgid "Support"
msgstr ""

#: gde-functions.php:346
msgid "Please review the documentation before submitting a request for support:"
msgstr ""

#: gde-functions.php:349
msgid "Plugin FAQ"
msgstr ""

#: gde-functions.php:350
msgid "Support Forum"
msgstr ""

#: gde-functions.php:353
msgid "If you're still experiencing a problem, please complete the form below."
msgstr ""

#: gde-functions.php:359
msgid "Your Name"
msgstr ""

#: gde-functions.php:363
msgid "Your E-mail"
msgstr ""

#: gde-functions.php:369
msgid "Shortcode"
msgstr ""

#: gde-functions.php:371
msgid "If you're having a problem getting a specific document to work, paste the shortcode you're trying to use here."
msgstr ""

#: gde-functions.php:376
msgid "Message"
msgstr ""

#: gde-functions.php:381
msgid "Message Options"
msgstr ""

#: gde-functions.php:383
msgid "Send debug information"
msgstr ""

#: gde-functions.php:384
msgid "View"
msgstr ""

#: gde-functions.php:385
msgid "Send me a copy"
msgstr ""

#: gde-functions.php:391
msgid "Debug Information"
msgstr ""

#: gde-functions.php:430
msgid "I'm less likely to be able to help you if you do not include debug information."
msgstr ""

#: gde-functions.php:432
msgid "Send Support Request"
msgstr ""

#: gviewer.php:137
msgid "Error"
msgstr ""

#: gviewer.php:268
#: gviewer.php:284
#: options.php:130
msgid "Settings"
msgstr ""

#: gviewer.php:275
msgid "You do not have sufficient permissions to access this page"
msgstr ""

#: gviewer.php:305
msgid "Beta version available"
msgstr ""

#: gviewer.php:305
msgid "Please deactivate the plug-in and install the current version if you wish to participate. Otherwise, you can turn off beta version checking in GDE Settings. Testers appreciated!"
msgstr ""

#: gviewer.php:306
msgid "Updated beta version available"
msgstr ""

#: gviewer.php:306
msgid "A newer beta has been released. Please deactivate the plug-in and install the current version. Thanks for your help!"
msgstr ""

#: gviewer.php:307
msgid "You're running a beta version. Please give feedback."
msgstr ""

#: gviewer.php:307
msgid "Thank you for running a test version of Google Doc Embedder. You are running the most current beta version. Please give feedback on this version using the &quot;Support&quot; link above. Thanks for your help!"
msgstr ""

#: gviewer.php:308
msgid "more info"
msgstr ""

#: gviewer.php:375
msgid "plugin"
msgstr ""

#: gviewer.php:376
msgid "Version"
msgstr ""

#: options.php:26
msgid "Options reset to defaults"
msgstr ""

#: options.php:123
msgid "Options updated"
msgstr ""

#: options.php:141
msgid "Viewer Options"
msgstr ""

#: options.php:146
msgid "Viewer Selection"
msgstr ""

#: options.php:147
#: options.php:253
#: options.php:303
msgid "Help"
msgstr ""

#: options.php:149
msgid "Google Standard Viewer"
msgstr ""

#: options.php:150
msgid "Embed the standard Google Viewer."
msgstr ""

#: options.php:151
msgid "Enhanced Viewer"
msgstr ""

#: options.php:152
msgid "Use this option to enable toolbar customization and fix some display problems (experimental)."
msgstr ""

#: options.php:156
msgid "Customize Toolbar"
msgstr ""

#: options.php:160
msgid "Hide Zoom In/Out"
msgstr ""

#: options.php:161
msgid "Hide Open in New Window"
msgstr ""

#: options.php:162
msgid "Always Use Mobile Theme"
msgstr ""

#: options.php:166
msgid "Default Size"
msgstr ""

#: options.php:167
#: libs/gde-dialog.php:61
msgid "Width"
msgstr ""

#: options.php:171
#: libs/gde-dialog.php:57
msgid "Height"
msgstr ""

#: options.php:177
msgid "Default Language"
msgstr ""

#: options.php:235
msgid "Inline (Default)"
msgstr ""

#: options.php:236
msgid "Collapsible (Open)"
msgstr ""

#: options.php:237
msgid "Collapsible (Closed)"
msgstr ""

#: options.php:249
msgid "Download Link Options"
msgstr ""

#: options.php:254
msgid "Display the download link by default"
msgstr ""

#: options.php:255
msgid "Only display download link to logged in users"
msgstr ""

#: options.php:256
msgid "Track downloads in Google Analytics (tracking script must be installed on your site)"
msgstr ""

#: options.php:259
msgid "File Base URL"
msgstr ""

#: options.php:261
msgid "Any file not starting with <em>http</em> will be prefixed by this value"
msgstr ""

#: options.php:264
msgid "Link Text"
msgstr ""

#: options.php:266
msgid "You can further customize text using these dynamic replacements:"
msgstr ""

#: options.php:267
msgid "filename"
msgstr ""

#: options.php:268
msgid "file type"
msgstr ""

#: options.php:269
msgid "file size"
msgstr ""

#: options.php:272
msgid "Link Position"
msgstr ""

#: options.php:274
msgid "Above Viewer"
msgstr ""

#: options.php:275
msgid "Below Viewer"
msgstr ""

#: options.php:280
msgid "Link Behavior"
msgstr ""

#: options.php:282
msgid "Browser Default"
msgstr ""

#: options.php:283
msgid "Force Download"
msgstr ""

#: options.php:284
msgid "Force Download (Mask URL)"
msgstr ""

#: options.php:298
msgid "Advanced Options"
msgstr ""

#: options.php:300
msgid "Plugin Behavior"
msgstr ""

#: options.php:301
msgid "Editor Behavior"
msgstr ""

#: options.php:305
msgid "Display error messages inline (not hidden)"
msgstr ""

#: options.php:306
msgid "Disable internal error checking"
msgstr ""

#: options.php:307
msgid "Disable document caching"
msgstr ""

#: options.php:308
msgid "Disable beta version notifications"
msgstr ""

#: options.php:311
msgid "Disable all editor integration"
msgstr ""

#: options.php:316
msgid "Insert shortcode from Media Library"
msgstr ""

#: options.php:318
msgid "Allow uploads of all supported media types"
msgstr ""

#: options.php:328
msgid "Save Options"
msgstr ""

#: options.php:330
msgid "Reset to Defaults"
msgstr ""

#: options.php:330
msgid "Are you sure you want to reset all settings to defaults?"
msgstr ""

#: proxy.php:29
msgid ""
"This function is not supported on your web server. Please add\n"
"\t\t\t<code>allow_url_fopen = 1</code> to your php.ini or enable cURL library.\n"
"\t\t\tIf you are unable to do this, please switch to Google Standard Viewer in GDE Options."
msgstr ""

#: libs/bootstrap.php:15
msgid "Could not find wp-load.php"
msgstr ""

#: libs/gde-dialog.php:34
msgid "I'll insert the shortcode myself"
msgstr ""

#: libs/gde-dialog.php:36
msgid "GDE Shortcode Options"
msgstr ""

#: libs/gde-dialog.php:40
msgid "Required"
msgstr ""

#: libs/gde-dialog.php:43
msgid "URL or Filename"
msgstr ""

#: libs/gde-dialog.php:43
msgid "Full URL or filename to append to File Base URL"
msgstr ""

#: libs/gde-dialog.php:45
msgid "File Base URL will be prefixed"
msgstr ""

#: libs/gde-dialog.php:46
msgid "Unsupported file type"
msgstr ""

#: libs/gde-dialog.php:54
msgid "Optional (Override Global Settings)"
msgstr ""

#: libs/gde-dialog.php:57
#: libs/gde-dialog.php:61
msgid "format:"
msgstr ""

#: libs/gde-dialog.php:57
#: libs/gde-dialog.php:61
msgid "or"
msgstr ""

#: libs/gde-dialog.php:65
msgid "Show Download Link"
msgstr ""

#: libs/gde-dialog.php:66
msgid "Yes"
msgstr ""

#: libs/gde-dialog.php:66
msgid "No"
msgstr ""

#: libs/gde-dialog.php:71
msgid "Show download link only if user is logged in"
msgstr ""

#: libs/gde-dialog.php:77
msgid "Disable caching (this document is frequently overwritten)"
msgstr ""

#: libs/gde-dialog.php:84
msgid "Disable internal error checking (try if URL is confirmed good but document doesn't display)"
msgstr ""

#: libs/gde-dialog.php:94
msgid "Shortcode Preview"
msgstr ""

#: libs/gde-dialog.php:103
msgid "Insert"
msgstr ""

#: libs/gde-dialog.php:107
msgid "Cancel"
msgstr ""

