=== Plugin Name ===
Contributors: lizeipe 
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=W884YAWEDPA9U
Tags: CSV , Webpage , Wordpress , Plugin,Excel Data
Requires at least: 2.0.2
Tested up to: 3.0
Stable tag: 4.3

CSV (Comma Separated Value) to Webpage plugin help to upload data in a CSV (excel) file to webpage in one click.

== Description ==

CSV to Webpage plugin help to upload data in a CSV (Excel File) file to webpage in one click.It is easy to use and simple to implement.It is possible to upload CSV content to webpage in table format.

This plugin is very helpful for those who need to display dynamic data from their excel sheet to their website as a table.Using this plugin it is possible to upload the file from backend to a specified location.There is options to set the style to display tables from the backend .

CSV to Webpage plugin will come very handy if you manage a sports website in which you need to update scores, points table etc. regularly. Your updated excel file will go live on to your website with just few clicks of the mouse.

Live Demo - <a href="http://pearlbells.co.uk/joomlaportfolio/6-csv-file-upload.html">Click here</a>

== Installation ==

1. Download the zip file .
2. Login to wordpress admin area.
3. select plugin -> add new
4. select `upload` from top menu on the page.
5. click `browse` and select the zip file.
6. click 'install now'
7. click `activate the plugin`
8. Place your CSV file to `/wp-content/plugins/pearl_csv_to_webpage/upload/` folder.
9. Go to the webpage where you want to display the CSV content and enter `[pearl_csv_to_webpage_display filename = yourfilename.csv]`


== Frequently Asked Questions ==

= Can I display data from two CSV files in one webpage ? =

  Yes it is possible .You should include the plugin shortcode two times as shown below.
  [pearl_csv_to_webpage_display filename = yourfilename1.csv]
  [pearl_csv_to_webpage_display filename = yourfilename2.csv]

= Can I customise the tables to match with the look and feel of my website ? =

   Yes you can set styles from the admin area. 


== Screenshots ==

1. This screen shot gives a demo of how data in a CSV file will appear on a webpage
2. This is the second screen shot

== Changelog ==

= 1.0 =
Initial Release
= 1.1 =
* Fixed Documentation Error - step 5

== Upgrade Notice ==

= 1.0 =
Upgrade notices describe the reason a user should upgrade.  No more than 300 characters.

= 0.5 =
This version fixes a security related bug.  Upgrade immediately.

