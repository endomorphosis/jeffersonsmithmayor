<?php
/**
 * Template Name: Home
 * Description: Template for the home
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'home' ); ?>

				<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->
			<div class="home-sidebar top">
				<?php dynamic_sidebar( 'home-top' );?>
			</div>
			
		</div><!-- #primary -->

<?php get_sidebar('sidebar'); ?>
<div style="clear:both"></div>
<div class="home-sidebar bottom">
	<?php dynamic_sidebar( 'home' );?>
</div>
<?php get_sidebar('youtube'); ?>
<?php get_sidebar('photos'); ?>

<?php get_footer(); ?>