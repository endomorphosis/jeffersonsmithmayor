<?php
/**
 * Template Name: FAQ
 * Description: A Page Template that adds a sidebar to pages
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'stripped' ); ?>
				<?php endwhile; // end of the loop. ?>
				<?php $faq_pages = array(544,542,546,571,537,539,532,535); ?>
				<?php foreach($faq_pages as $page):?>
					<?php
					$id = $page;
					$parent = get_post_ancestors($id);
					$parent = $parent[count($parent)-1];
					if( $parent == null ) {
						$dude = wp_list_pages("title_li=&include=".$id.'&echo=0');
						$children = wp_list_pages("title_li=&child_of=".$id.'&echo=0');
					} else {
						$dude = wp_list_pages("title_li=&include=".$parent.'&echo=0');
						$children = wp_list_pages("title_li=&child_of=".$parent.'&echo=0');
					}
					if( !empty($children) && !is_front_page() && !is_404() ) {
						echo '<ul class="title">'.$dude.'</ul>';
						echo '<ul class="child-links">'.$children.'</ul>';
					}
					?>
				<?php endforeach; ?>
			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>