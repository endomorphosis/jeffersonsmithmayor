<?php
/**
 * The  containing the main widget area.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

$options = twentyeleven_get_theme_options();
$current_layout = $options['theme_layout'];

if ( 'content' != $current_layout ) :
?>
<div id="youtube-bar" class="bar">
	<div class="inner">
		<h1>Jeff on the Tubes</h1>
		<?php dynamic_sidebar( 'video' );?>
	</div>
	<div class="prev off"></div>
	<div class="next"></div>
	<div style="clear:both;"></div>
</div>
<div class="left_arrow youtube_bar_left"  class="arrow"></div>
<div class="right_arrow youtube_bar_right" class="arrow"></div>

<?php endif; ?>
