<?php

// This action will add the new sidebars
add_action( 'widgets_init', 'ul_widgets_init' );

// This is the function for adding the sidebar
function ul_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Home Sidebar Bottom', 'ul' ),
		'id' => 'home',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Home Sidebar Top', 'ul' ),
		'id' => 'home-top',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Photo Sidebar', 'ul' ),
		'id' => 'photo',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Video Sidebar', 'ul' ),
		'id' => 'video',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Issues', 'ul' ),
		'id' => 'issues',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Volunteer HQ', 'ul' ),
		'id' => 'volunteer',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}

function my_header_image_height($height) {
	return 60;
}
add_filter( 'twentyeleven_header_image_height', 'my_header_image_height');

remove_filter( 'HEADER_IMAGE_HEIGHT', 'twentyeleven_header_image_height' );

define( 'CHILD_HEADER_IMAGE_HEIGHT', apply_filters( 'child_header_image_height', 60 ) );




class bettermenu_widget extends WP_Widget
{
  function bettermenu_widget()
  {
    $widget_ops = array('description' => __('Displays a Page\'s Sibling and Parent Menu Items'));
    $this->WP_Widget('bettermenu', __('Related Pages Menu'), $widget_ops);
  }

  function form($instance)
  {

  }

  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;

    return $instance;
  }

  function widget($args, $instance)
  {
		$id = get_the_ID();
		if( is_home() ) $id = get_option('page_for_posts');
		$parent = get_post_ancestors($id);
		$parent = $parent[count($parent)-1];
		if( $parent == null ) {
			$dude = wp_list_pages("title_li=&include=".$id.'&echo=0');
			$children = wp_list_pages("title_li=&child_of=".$id.'&echo=0');
		} else {
			$dude = wp_list_pages("title_li=&include=".$parent.'&echo=0');
			$children = wp_list_pages("title_li=&child_of=".$parent.'&echo=0');
		}
		if( !empty($children) && !is_front_page() && !is_404() ) {
			echo '<aside class="widget widget_text"> <div class="better_menu a_button">';
			echo '<ul class="title">'.$dude.'</ul>';
			echo '<ul class="child-links">'.$children.'</ul>';
			echo '</div><div class="join_arrow"></div></aside>';
		}
  }
}
add_action('widgets_init', create_function('', 'return register_widget("bettermenu_widget");'));


add_action('widgets_init', create_function('', 'return register_widget("jolokia_better_recent_posts");'));


// Widget that does a better version of the Recent Posts, displays an excerpt and 
class jolokia_better_recent_posts extends WP_Widget
{
  function jolokia_better_recent_posts()
  {
    $widget_ops = array('description' => __('Better Recent Posts, with Jolokia Styling.'));
    $this->WP_Widget('jolokia_better_recent_posts', __('Better Recent Posts'), $widget_ops);
  }

  function form($instance)
  {
    $category_id = isset($instance['category_id']) ?esc_attr($instance['category_id']) : '';
    $blog_page =  isset($instance['blog_page']) ? esc_attr($instance['blog_page']) : '';
    $post_number =  isset($instance['post_number']) ? esc_attr($instance['post_number']) : 3;
    $share_text = isset($instance['share_text']) ? esc_attr($instance['share_text']) : '';
    $title_text = isset($instance['title_text']) ? esc_attr($instance['title_text']) : '';
	$share = empty($share_text) ? 'Read the Blog' : $share_text;
    ?>
	<p>
		<label for="<?php echo $this->get_field_id('category_id'); ?>"> <?php echo __('If you\'d like to only pull recent posts from certain categories, enter their ids separated by a comma here:') ?>
			<input id="<?php echo $this->get_field_id('category_id'); ?>" name="<?php echo $this->get_field_name('category_id'); ?> type="text" value="<?php echo $category_id;?>">
		</label>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id('title_text'); ?>"> <?php echo __('<strong>OPTIONAL:</strong> The Title of the Widget (if blank will just say "News") ') ?>
			<input id="<?php echo $this->get_field_id('title_text'); ?>" name="<?php echo $this->get_field_name('title_text'); ?> type="text" value="<?php echo $title_text;?>">
		</label>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id('share_text'); ?>"> <?php echo __('<strong>OPTIONAL:</strong> Otherwise will just say "Read the Blog") ') ?>
			<input id="<?php echo $this->get_field_id('share_text'); ?>" name="<?php echo $this->get_field_name('share_text'); ?> type="text" value="<?php echo $share_text;?>">
		</label>
	</p>
		<p>The "<?php echo $share; ?>" button will automatically link to your "Post Page". Set up your post page <a href="<?php echo admin_url(); ?>options-reading.php">here</a>.</p>
	<p>
		<label for="<?php echo $this->get_field_id('post_number'); ?>"> <?php echo __('The number of posts to display') ?>
			<input id="<?php echo $this->get_field_id('post_number'); ?>" name="<?php echo $this->get_field_name('post_number'); ?> type="text" value="<?php echo $post_number;?>">
		</label>
	</p>
   <?php
  }

  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['category_id'] = strip_tags($new_instance['category_id']);
    $instance['post_number'] = strip_tags($new_instance['post_number']);
    $instance['share_text'] = strip_tags($new_instance['share_text']);
    $instance['title_text'] = strip_tags($new_instance['title_text']);

    return $instance;
  }

  function widget($args, $instance)
  {
	extract($args);
	$category_id  = isset($instance['category_id']) && ( $instance['category_id'] != '' ) ? esc_attr($instance['category_id']) : '';
	$post_number  = isset($instance['post_number']) && ( $instance['post_number'] != '' ) ? esc_attr($instance['post_number']) : 3;
	$title_text  = isset($instance['title_text']) && ( $instance['title_text'] != '' ) ? esc_attr($instance['title_text']) : 'NEWS';
	$share_text  = isset($instance['share_text']) && ( $instance['share_text'] != '' ) ? esc_attr($instance['share_text']) : 'READ THE BLOG';


	echo $before_widget; ?>
	<h3 class="widget-title"><?php echo $title_text; ?> <a href="<?php echo get_permalink(get_option('page_for_posts')); ?>" class="more" style="margin-top:-12px" ><?php echo $share_text; ?></a></h3>
	<?php

	$args = array(
		'post_type' => 'post',
		'posts_per_page' => $post_number,
		'orderby'=>'date'
	);

	if( !empty($category_id) ) array_merge( $args, array('cats' => $category_id) );
	$blog = get_permalink(get_option('page_for_posts'));
    // Output the query to find the testimonial
    query_posts( $args );
    while ( have_posts() ) : the_post(); ?>
			<div id="recent-posts" >
				<a href="<?php echo $blog.'#'; the_ID(); ?>"><strong><?php echo ucwords(the_title()); ?></strong></a>
			<?php echo the_excerpt(); ?>
			</div>
	<?php endwhile;
    wp_reset_query();

    // Output $after_widget
    echo $after_widget;
  }
}
function add_form_js() {
	wp_enqueue_script('forms',get_bloginfo('stylesheet_directory').'/forms.js',array('jquery'),1,true);
}
add_action('wp_head', 'add_form_js');

require_once('rotator/plugin.php');


?>