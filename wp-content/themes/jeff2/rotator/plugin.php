<?php
/*
Plugin Name: Header Rotator
Plugin URI: https://github.com/Mojowen/HeaderRotator
Description: SalsaPress connects WordPress to Salsa
Author: Scott Duncombe
Version: 0.1
Author URI: http://scottduncombe.com/
*/

//$base = WP_PLUGIN_URL . '/' . str_replace(basename( __FILE__), "" ,plugin_basename(__FILE__));
$base = get_bloginfo('stylesheet_directory').'/rotator';
define('rotator_base', $base);

require_once('admin_page.php');
$rotator_array = get_option('rotator_options') ? get_option('rotator_options') : array();

if( isset($rotator_array['activate']) ){
	require_once('rotator.php');
}
?>