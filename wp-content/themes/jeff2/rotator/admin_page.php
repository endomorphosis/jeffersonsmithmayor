<?php


function my_admin_scripts() {
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	wp_register_script('my-upload',  rotator_base.'/admin_js.js', array('jquery','media-upload','thickbox','jquery-ui-sortable','farbtastic'));
	wp_enqueue_script('my-upload');
}

function my_admin_styles() {
	wp_enqueue_style('thickbox');
	wp_enqueue_style('farbtastic');
	//wp_enqueue_style( 'theme_options', get_bloginfo('template_directory').'/custom/theme_options.css','', '0.5', 'all' );
}

if ( isset($_GET['page']) && $_GET['page'] == 'header_rotator') {
	add_action('admin_print_scripts', 'my_admin_scripts');
	add_action('admin_print_styles', 'my_admin_styles');
}

// Will add the Rotator Menu
add_action('admin_menu', 'rotator_theme_menu');

function rotator_theme_menu() {
	// Adds subtheme menu
	add_submenu_page(
		'themes.php',
		'Configure Theme',
		'Header Rotator',
		'manage_options',
		'header_rotator',
		'header_rotator' );
}

function header_rotator() {
	// This is the function that runs when you visit the page
	if (!current_user_can('manage_options'))  { wp_die( __('You do not have sufficient permissions to access this page.') ); }
	?>

	
		<form autocomplete='off' action="options.php" method="post">
			<?php settings_fields('rotator_options'); ?>
			<?php do_settings_sections(__FILE__); ?>
			<p class="submit">
				<input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
			</p>
		</form>
	<?php
}

// This adds the rotator options
add_action('admin_init', 'rotator_options' );

function rotator_options() {
	register_setting('rotator_options', 'rotator_options');
	add_settings_section('rotator_options', 'Rotator Options', 'rotator_options_section', __FILE__);
	add_settings_field('activate', 'Activate Rotator', 'rotator_activated', __FILE__, 'rotator_options');
	add_settings_field('speed', 'Rotator Speed', 'rotator_speed', __FILE__, 'rotator_options');
	add_settings_field('headers', 'Headers to Rotat', 'rotator_headers', __FILE__, 'rotator_options');
	
}
function rotator_options_section() { 
	echo 'Activate and customize the header rotator\'s options';
}
function rotator_activated() {
	$options = get_option('rotator_options');
	$checked = isset($options['activate']) && $options['activate'] == 'on' ? 'checked' : '';
	echo '<input name="rotator_options[activate]" type="checkbox" '.$checked.' value="on">';
}
function rotator_speed() {
	$options = get_option('rotator_options');
	$speed = isset($options['speed']) ? $options['speed'] : 5000;
	echo '<input name="rotator_options[speed]" type="text"  value="'.$speed.'" size="6">'.' in milliseconds';
}
function rotator_headers() {
	$options = get_option('rotator_options');
	echo '<ul id="sortable">';
	$i = 1;
	
	if( count($options['headers']) > 0):
		foreach( $options['headers'] as $o ):
			echo '<li style="width: 60%;margin: 0 0 35px;">';
			echo '<h2 style="width: 30px;float: left;margin-left: -30px;">&#8616;</h2><p>Header Image:<input value="'.$o['img'].'" class="upload_image" type="text" size="36" name="rotator_options[headers]['.$i.'][img]" value="" /><input class="upload_image_button" type="button" value="Upload Image" /></p>';
			echo '<p>Heading:<input  type="text"  name="rotator_options[headers]['.$i.'][heading]" value="'.$o['heading'].'"></p>';
			echo '<p>Link: <input  type="text"  name="rotator_options[headers]['.$i.'][link]" value="'.$o['link'].'"></p>';
			echo '<p>Text Style: <input  type="text"  name="rotator_options[headers]['.$i.'][style]" value="'.$o['style'].'"></p>';
			echo '<p>A attributes: <input  type="text"  name="rotator_options[headers]['.$i.'][other]" value="'.$o['other'].'"> things like style=\'\' target=\'\', etc</p>';
			echo '<div style="background: #FFF;float: right;margin-right: -70px;margin-top: -155px;">Body:<br ><textarea rows="4" columns="400" name="rotator_options[headers]['.$i.'][body]">'.$o['body'].'</textarea></div>';
			echo ' <a href="#" class="delete">delete</a>';
			echo '</li>';
			$i++;
		endforeach;
	else:
		echo '<li style="width: 60%;margin: 0 0 35px;">';
		echo '<h2 style="width: 30px;float: left;margin-left: -30px;">&#8616;</h2><p>Header Image:<input value="" class="upload_image" type="text" size="36" name="rotator_options[headers][0][img]" value="" /><input class="upload_image_button" type="button" value="Upload Image" /></p>';
		echo '<p>Heading: <input  type="text" name="rotator_options[headers][0][heading]" value=""></p>';
		echo '<p>Link: <input  type="text" name="rotator_options[headers][0][link]" value=""></p>';
		echo '<p>Text Style:<input  type="text" name="rotator_options[headers][0][style]" value="top:5px; left:484px; width:336px; height:0; color:#FFF; background-color:#000;"></p>';
		echo '<p>A attributes: <input  type="text" name="rotator_options[headers][0][other]" value=""> things like style=\'\' target=\'\', etc</p>';
		echo '<div style="background: #FFF;float: right;margin-right: -70px;margin-top: -155px;">Body:<br ><textarea rows="4" columns="400" name="rotator_options[headers][0][body]"></textarea></div>';
		echo ' <a href="#" class="delete">delete</a>';
		echo '</li>';
	endif;

	echo '<li><a href="#" class="add">Add New</a></li>';
	echo '</ul>';
}

?>