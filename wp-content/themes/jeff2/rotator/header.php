<?php 	$options = get_option('rotator_options'); ?>
<?php 	$headers = $options['headers']; ?>

<div class="header-rotator">
	<div class="wt-rotator">
    	<div class="screen">
            <noscript>
            		<?php foreach($headers as $o): ?>
					<?php if( isset($o['img']) && !empty($o['img']) ): ?>

						<img src="<?php echo $o['img'];?>"/>

						<?php break; ?>

					<?php endif; ?>
					<?php endforeach; ?>
            </noscript>
      	</div>
        <div class="c-panel">
      		<div class="thumbnails">
                <ul>
					<?php foreach($headers as $o): ?>
					<?php if( isset($o['img']) && !empty($o['img']) ): ?>
                    <li>
                        	<a href="<?php echo $o['img'];?>" >
								<img src="<?php echo $o['img'];?>"/>
							</a>
						
						<?php if( isset($o['link']) && !empty($o['link']) ): ?>
                        	<a href="<?php echo $o['link'];?>" 
								<?php if( isset($o['other']) && !empty($o['other']) ) echo $o['other'];?>
							></a>
						<?php endif; ?>
						<?php if( isset($o['heading']) && !empty($o['heading']) || isset($o['body']) && !empty($o['body'])   ): ?>
	                        <div style="<?php echo $o['style'];?>">
								<?php if( isset($o['heading']) && !empty($o['heading']) ): ?>
	                           		<h1><?php echo $o['heading'];?></h1>
								<?php endif; ?>
								<?php if( isset($o['body']) && !empty($o['body']) ) echo $o['body'];?>
	                       	</div>
						<?php endif; ?>
						
                    </li>
				<?php endif; ?>
				<?php endforeach; ?>
          	</div>     
  			<div class="buttons">
            	<div class="prev-btn"></div>
                <div class="play-btn"></div>    
            	<div class="next-btn"></div>               
            </div>
        </div>
    </div>	
</div>