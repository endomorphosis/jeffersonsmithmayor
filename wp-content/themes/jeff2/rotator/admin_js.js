jQuery(document).ready(function($) {

$('.upload_image_button').click(function() {
	$(this).prevAll('.upload_image').addClass('send_it_here');
	formfield = $('.upload_image').attr('name');
	tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
	return false;
});

window.send_to_editor = function(html) {
	imgurl = $('img',html).attr('src');
	$('.send_it_here').val(imgurl).removeClass('send_it_here');
	tb_remove();
}

$( "#sortable" ).sortable({ items: "li:not(:last)" });
$( "#sortable" ).disableSelection();

$('#sortable li a.delete').live('click', function(e) { e.preventDefault(); 
	if( $(this).parents('ul').find('li:not(:last)').length == 1 ) {
		$(this).parent('li').find('input[type=text],textarea').val('')
		$(this).parent('li').find('.preview').remove();
	} else {
		$(this).parent('li').remove(); 
	}
});

$('#sortable li a.add').click( function(e) { 
	e.preventDefault(); 
	var elem = $(this).parents('ul').find('li:first').clone();
	$('input[type=text],textarea',elem).each(function() {
		$(this).attr('name',$(this).attr('name').replace(/[0-9]/,($('#sortable li').length)) ).val('')
	});
	$('.preview',elem).remove();
	$(elem).insertBefore( $(this).parents('li') );
});

$(document).on({
	click: function(e) { $(this).focus() },
	dblclick: function(e) { $(this).select() }
}, '#sortable input');


});
