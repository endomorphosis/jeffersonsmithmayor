<?php



// Adding scripts for the rotator
add_action('wp_enqueue_scripts','ul_add_scripts');

// Function that is called w/ wp_enqueue_scripts
function ul_add_scripts() {
	// http://codex.wordpress.org/Function_Reference/wp_enqueue_script
	wp_enqueue_script('rotator',rotator_base.'/jquery.wt-rotator.js',array('jquery'),1,true);
	wp_enqueue_script('rotator',rotator_base.'/jqueryÏ.easing.1.3.min.js',array('jquery'),1,true);
	wp_enqueue_style( 'theme_options', rotator_base.'/wt-rotator.css','', '0.5', 'all' );
}

// Adding code for the header
add_action('wp_head', 'ul_add_header_json');

// Function that is w/ wp_header
function ul_add_header_json() {
	global $_wp_default_headers;
	
	$rotator_array = get_option('rotator_options') ? get_option('rotator_options') : array();
	$speed = isset($rotator_array['speed']) ? $rotator_array['speed'] : 5000;
	$array = isset($rotator_array['header']) ? $rotator_array['header'] : 'TwentyEleven';
	switch( $array ) {
		case 'TwentyEleven':
			$headers = $_wp_default_headers;
			foreach ($headers as $key => $value) {;
				$headers[$key]['url'] = str_replace('%s',get_bloginfo('template_directory'),$value['url']);
			}
			break;
		case 'custom':
			$headers = get_uploaded_header_images();
			break;
		default:
			$headers = get_uploaded_header_images();
			break;
	}
	if( is_front_page() ):
	// Injecting some javascript
	echo "<script type='text/javascript'>";
	// Injecting an object which contains some information: the width of the default headers and the headers urls
	echo 'jQuery(document).ready(	
		function($) {
			$(".header-rotator").wtRotator({
				width:1000,
				height:280,
				thumb_width:24,
        		thumb_height:24,
				button_width:24,
				button_height:24,
				button_margin:5,
				auto_start:true,
				delay:'.$speed.',
				play_once:false,
				transition:"fade",
				transition_speed:800,
				auto_center:true,
				easing:"",
				cpanel_position:"inside",
				cpanel_align:"BR",
				timer_align:"top",
				display_thumbs:true,
				display_dbuttons:true,
				display_playbutton:true,
				display_thumbimg:false,
       			display_side_buttons:false,
				display_numbers:true,
				display_timer:true,
				mouseover_select:false,
				mouseover_pause:false,
				cpanel_mouseover:false,
				text_mouseover:false,
				text_effect:"fade",
				text_sync:true,
				tooltip_type:"text",
				shuffle:false,
				block_size:75,
				vert_size:55,
				horz_size:50,
				block_delay:25,
				vstripe_delay:75,
				hstripe_delay:180			
			});
		}
	);';
	echo "</script>";
	endif;
}

?>