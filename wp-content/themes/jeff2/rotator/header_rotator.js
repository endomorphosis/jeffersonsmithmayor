// This script will be loaded in the footer and activate when the document is ready
jQuery(document).ready( function($) { 
	// We're checking to make sure the variables we set w/ ul_add_header_json are present
	if( typeof header_rotator != 'undefined' ) {


		// Grabs the header using the width to select it
		// Also setting variable we'll use to keep track of which header is 
		var header = $('header img[width='+header_rotator.width+']'),
			headers = [],
			selected = 0,
			count = -1;
		header.parent().css('height',header.css('height')+'px')
		// TO DO: Need to do something to make sure it doesn't rotate when there's a header uploaded for a particular page
		
		// Prepping the other images, we're going to:
		// -clone the header img
		// - sub in the new srcs from header_rotator.headers
		// - set css display: none;
		// - append them to the documenet so the browser will fetch them for us

		// Usually we'd do this with for( var i=0; i < header_rotator.headers.length; i++ )
		// But the JSON gives us an object, not an array, so we have to treat it different
		for( var i in header_rotator.headers) {
			// Makes sure the header isn't the one that's currently displayed
			if( header_rotator.headers[i].url != header.attr('src') ) {
				// here we're adding the new headers to the DOM so that the browser will load them
				var new_header = header.clone();
				new_header.css('display','none').attr('src',header_rotator.headers[i].url);
				header.parent().append(new_header);
			}
			// Here we're building a js array using the header urls, it'll be easier to access later on
			headers.push(header_rotator.headers[i].url);
			count += 1;
		};
		
		// An interval that rotats the image by swapping out the source and doing a small fade animation using opacity
		var change = setInterval( function() { 

			selected += 1; // The counter, makes sure that we loop when we get to the last image
			if( selected > count ) { selected = 0; } 
			$header = $('header img[width='+header_rotator.width+']:visible');
			// Checks if paused
			if( !$header.is(':hover') ) {
				$header.animate( // Beggining the animation
					{ opacity: 0.1}, // The characteristic to change
					header_rotator.speed/4, // The speed
					function() { // The callback function that's called when animation is complete
						$(this).attr('src',headers[selected]).animate( // Swaps out the img src with one of the other images and calls animate again
							{opacity: 1 }, // Makes it fully visible
							header_rotator.speed/4 // The speed, no need for a callback 
						);
					}
				);
			}
		}, 
		header_rotator.speed ); // This is the timing for the whole interval
	}

});