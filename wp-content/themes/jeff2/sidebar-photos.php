<?php
/**
 * The  containing the main widget area.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

$options = twentyeleven_get_theme_options();
$current_layout = $options['theme_layout'];

if ( 'content' != $current_layout ) :
?>
<div id="photo-bar" class="bar">
	<div class="inner">
		<h1>PICS FROM THE CAMPAIGN TRAIL</h1>
		<?php dynamic_sidebar( 'photo' );?>
	</div>
	<div class="prev off"></div>
	<div class="next"></div>
	<div style="clear:both;"></div>
</div>

<?php endif; ?>
