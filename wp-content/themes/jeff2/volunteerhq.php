<?php
/**
 * Template Name: Volunteer HQ
 * Description: A Page Template that adds a sidebar to pages
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

		<div id="primary">
			<style type="text/css">#text-5 { display: none } </style>
			<div id="content" role="main">
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'stripped' ); ?>
				
				<?php endwhile; // end of the loop. ?>
				
			</div><!-- #content -->
			<div class="sub-sidebar top">
				<?php dynamic_sidebar( 'volunteer-hq' );?>
			</div>
		</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>