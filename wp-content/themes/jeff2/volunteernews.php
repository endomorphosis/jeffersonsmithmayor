<?php
/**
 * Template Name: Volunteer News
 * Description: A Page Template for all news in volunteer-news category
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); 
$query = new WP_Query( 'category_name=volunteer-news' );
?>

<section id="primary">
	<div id="content" role="main">

	<?php if ( $query->have_posts() ) : ?>

		<?php twentyeleven_content_nav( 'nav-above' ); ?>

		<?php /* Start the Loop */ ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>

			<?php
				/* Include the Post-Format-specific template for the content.
				 * If you want to overload this in a child theme then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );
			?>

		<?php endwhile; ?>

		<?php twentyeleven_content_nav( 'nav-below' ); ?>

	<?php endif; ?>

	</div><!-- #content -->
</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>