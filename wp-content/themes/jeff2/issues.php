<?php
/**
 * Template Name: Issues
 * Description: A Page Template that adds a sidebar to pages
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'stripped' ); ?>
				<?php endwhile; // end of the loop. ?>
				
			</div><!-- #content -->
			<div class="sub-sidebar top">
				<?php dynamic_sidebar( 'issues' );?>
			</div>
			<div style="clear: both; padding: 30px 60px">Or skip straight to our most <h2 style="display: inline-block"><a title="FAQ" href="http://jeffersonsmith.com/faq/">frequently asked questions</a></h2></div>
		</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>